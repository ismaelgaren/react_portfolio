import React, { useState, useEffect } from 'react';
import './App.css';
import Navbar from './components/Navbar';
import Header from './pages/Header';
import About from './pages/About';
import Skills from './pages/Skills';
import Education from './pages/Education';
import Experience from './pages/Experience';
import Project from './pages/Project';
import Contact from './pages/Contact';
import Footer from './pages/Footer';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';

function App() {
  const [isLoading, setLoading] = useState(true);

  function fakeRequest(){
    return new Promise(resolve => setTimeout(() => resolve(), 2500));
  }

  useEffect(() => {
    fakeRequest().then(() => {
      const el = document.querySelector('.loader-container');
      if(el){
        el.remove();
        setLoading(!isLoading);
      }
    })
  });

  if(isLoading) {
    return null;
  }

  return (
    <div>
      <Router>
        <Navbar />
      <Switch>
        <Route exact path="/" component={Header} />
        <Route path="/about" component={About} />
        <Route path="/skills" component={Skills} />
        <Route path="/education" component={Education} />
        <Route path="/experience" component={Experience} />
        <Route path="/project" component={Project} />
        <Route path="/contact" component={Contact} />
      </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
