import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faMobile } from '@fortawesome/free-solid-svg-icons';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';

export default function Contact(){

	const mapMarker = <FontAwesomeIcon icon={faMapMarkerAlt} size="2x"/>
	const mobile = <FontAwesomeIcon icon={faMobile} size="2x" />
	const linkedin = <FontAwesomeIcon icon={faLinkedinIn} size="2x" />
	const envelope = <FontAwesomeIcon icon={faEnvelope} size="2x" />

	return(
		<section className="bg-light" id="contact">
	      <div className="container">
	        <header className="mb-3 pb-4">
	          <p className="font-weight-bold text-primary text-uppercase letter-spacing-3">Call me, maybe.</p>
	          <h2 className="text-uppercase lined">Contact</h2>
	        </header>
	        <div className="row">
	          <div className="col-lg-3 col-md-6 mb-4 mb-lg-0">
	            <div className="px-4 py-5 text-center contact-item shadow-sm">{mapMarker}
	              <h4 className="contact-item-title h5 text-uppercase">Location</h4>
	              <p className="text-small mb-0">Malabon, Metro Manila</p>
	            </div>
	          </div>
	          <div className="col-lg-3 col-md-6 mb-4 mb-lg-0">
	            <div className="px-4 py-5 text-center contact-item shadow-sm">{mobile}
	              <h4 className="contact-item-title h5 text-uppercase">Phone</h4>
	              <p className="text-small mb-0">0945 277 7896</p>
	            </div>
	          </div>
	          <div className="col-lg-3 col-md-6 mb-4 mb-lg-0"><a className="px-4 py-5 text-center contact-item shadow-sm d-block reset-anchor" href="https://www.linkedin.com/in/ismael-garen-3377711b9/">{linkedin}
	              <h4 className="contact-item-title h5 text-uppercase">Linked in</h4>
	              <p className="text-small mb-0">@ismaelgaren</p></a></div>
	          <div className="col-lg-3 col-md-6 mb-4 mb-lg-0"><a className="px-4 py-5 text-center contact-item shadow-sm d-block reset-anchor" href="https://mail.google.com/mail/u/0/#inbox">{envelope}
	              <h4 className="contact-item-title h5 text-uppercase">Email</h4>
	              <p className="text-small mb-0">jovygar1@gmail.com</p></a></div>
	        </div>
	      </div>
	    </section>
		)
}