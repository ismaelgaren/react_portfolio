import React from 'react';

export default function Experience(){
	return(
		<section id="experience">
	      <div className="container">
	        <header className="mb-3 pb-4">
	          <p className="font-weight-bold text-primary text-uppercase letter-spacing-3">Yes. I've been around.</p>
	          <h2 className="text-uppercase lined">Experience</h2>
	        </header>
	        <ul className="timeline">
	        	  <li className="timeline-item ml-3 pb-4">
	                <div className="timeline-arrow"></div>
	                <div className="row no-gutter">
	                  <div className="col-lg-5 mb-4 mb-lg-0">
	                    <p className="font-weight-bold mb-2 text-primary text-small">2020 – 2021 </p>
	                    <h2 className="h5 mb-0 text-uppercase">Zuitt Coding Bootcamp</h2>
	                    <p className="text-small mb-0">Bootcamper</p><span className="small text-muted">Makati, Metro Manila</span>
	                  </div>
	                  <div className="col-lg-7">
	                    <p className="text-muted">Studied Web Development such as HTML, CSS, JavaScript, JQuery, SASS, ReactJS, MEN Stack, and PHP</p>
	                  </div>
	                </div>
	              </li>
	              <li className="timeline-item ml-3 pb-4">
	                <div className="timeline-arrow"></div>
	                <div className="row no-gutter">
	                  <div className="col-lg-5 mb-4 mb-lg-0">
	                    <p className="font-weight-bold mb-2 text-primary text-small">2020 – 2021 </p>
	                    <h2 className="h5 mb-0 text-uppercase">JO-ES Publishing House Inc.</h2>
	                    <p className="text-small mb-0">Junior IT Staff</p><span className="small text-muted">Valenzuela, Metro Manila</span>
	                  </div>
	                  <div className="col-lg-7">
	                    <p className="text-muted">Troubleshooting, configuring, and installing hardwares, networks and softwares</p>
	                  </div>
	                </div>
	              </li>
	              <li className="timeline-item ml-3 pb-4">
	                <div className="timeline-arrow"></div>
	                <div className="row no-gutter">
	                  <div className="col-lg-5 mb-4 mb-lg-0">
	                    <p className="font-weight-bold mb-2 text-primary text-small">2019 - 2020 </p>
	                    <h2 className="h5 mb-0 text-uppercase">iValue Technologies Inc.</h2>
	                    <p className="text-small mb-0">Technical Support</p><span className="small text-muted">Makati, Metro Manila</span>
	                  </div>
	                  <div className="col-lg-7">
	                    <p className="text-muted">Installing, configuring and fixing PABX, and CCTV.</p>
	                  </div>
	                </div>
	              </li>
	        </ul>
	      </div>
	    </section>
		)
}