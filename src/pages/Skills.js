import React from 'react'

export default function SkillSets(){
  return(
    <section id="expertise">
      <div className="container">
        <header className="mb-3 pb-4">
          <p className="font-weight-bold text-primary text-uppercase letter-spacing-3">Batman would be jealous.</p>
          <h2 className="text-uppercase lined">Skill Sets</h2>
        </header>
        <div className="row">
          <div className="col-md-6 mb-3">
            <h3 className="h4"><span className="text-primary mr-2">01</span>Computer Hardware</h3>
            <p className="text-muted text-small ml-4 pl-3">Troubleshooting, installing, formatting, and configuring computer hardwares.</p>
          </div>
          <div className="col-md-6 mb-3 mb-md-0">
            <h3 className="h4"><span className="text-primary mr-2">02</span>Web Development</h3>
            <p className="text-muted text-small ml-4 pl-3">Developing website/applications using HTML, CSS, SASS, JavaScript, ReactJs, MERN Stack and PHP</p>
          </div>
          <div className="col-md-6">
            <h3 className="h4"><span className="text-primary mr-2">03</span>Computer Network</h3>
            <p className="text-muted text-small ml-4 pl-3">Troubleshooting, installing and configuring computer networks</p>
          </div>
          <div className="col-md-6">
            <h3 className="h4"><span className="text-primary mr-2">04</span>Make people laugh</h3>
            <p className="text-muted text-small ml-4 pl-3">By telling corny jokes, stories and doing nonsense stuff.</p>
          </div>
        </div>
      </div>
    </section>
    )
}