import React from 'react';

export default function About(){
	return(
		<section className="bg-light" id="about">
	      <div className="container">
	        <header className="mb-5">
	          <p className="font-weight-bold text-primary text-uppercase letter-spacing-3">Innovative solutions</p>
	          <h2 className="h3 lined">Aspiring Full Stack Web Developer</h2>
	        </header>
	        <p className="lead text-muted">I am <strong className="text-dark">Ismael Garen. </strong>  Building my own career as <strong className="text-dark">Full Stack Web Developer </strong>because I enjoy creating website using different kinds of programming language. I was on college when I first realized that I love programming. The first project I created was Invetory System for our School. I was assigned as Team Leader and Web Designer/Developer from front-end to back-end. I used HTML, CSS, JavaScript and PHP to build the webiste. I graduated as BSIT and find out that I want to be a web developer, but because of lacking financially, my parents aren't able to work because of their age and sick that's why I had to find a job ASAP so I started as a Branch Coordinator. Worked for 1 year. When I resigned, a company offered me a job as a Technical Sales Associate that is not related to programming. But again, because I am just the one who is working that time, I accepted it. Worked for 9 months as an Technical Sales Associate but the pandemic came so the management decided to give me a break. I have rested on working for almost 2 months before my current company offered me a job as a Junior IT Staff/Web Developer. I accepted it and fortunately, my sister has graduated and finished her study and became a Certified Public Accountant. That's the time I started to think to pursue my dream of becoming a web developer. Now, I am studying full stack web development in Zuitt Coding Bootcamp, and study wherever I am continiously. I love programming that's why I wanted to build my career on creating website applications.</p>
	        <p className="text-muted"> - Graduated as an BSIT last May, 2018.</p>
	        <p className="text-muted"> - Awarded as Best in Capstone arduino based.</p>
	        <p className="text-muted"> - Table Tennis Champion(doubles and singles).</p>
	      </div>
	    </section>
    )
}