import React from 'react';

export default function Footer(){

	return(
		<footer className="footer">
	      <div className="copyrights px-4">
	        <div className="container py-4 border-top text-center">
	          <p className="mb-0 text-muted py-2">Portfolio &copy; <a href="https://www.linkedin.com/feed/">Ismael Garen </a> </p>
	        </div>
	      </div>
	    </footer>
		
		)
}
