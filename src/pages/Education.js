import React from 'react';

export default function Education(){
	return(
		<section className="bg-light" id="education">
	      <div className="container">
	        <header className="mb-3 pb-4">
	          <p className="font-weight-bold text-primary text-uppercase letter-spacing-3">Lazy isn't in my vocabulary.</p>
	          <h2 className="text-uppercase lined">Education</h2>
	        </header>

	        {/* Timeline item */}
	        <ul className="timeline">
        		{/* Timeline item */}
    		   <li className="timeline-item ml-3 pb-4">
                <div className="timeline-arrow"></div>
                <div className="row no-gutter">
                  <div className="col-lg-5 mb-4 mb-lg-0">
                    <p className="font-weight-bold mb-2 text-primary text-small">2019 - 2020 </p>
                    <h2 className="h5 mb-0 text-uppercase">Web Developer</h2>
                    <p className="text-small mb-0">Zuitt Coding Bootcamp</p><span className="small text-muted">Enzo Building, Makati</span>
                  </div>
                  <div className="col-lg-7">
                    <p className="text-muted">Studied Full Stack Web Developer using MERN Stack, PHP, Laravel, HTML, CSS, and JavaScript.</p>
                  </div>
                </div>
              </li>
              <li className="timeline-item ml-3 pb-4">
                <div className="timeline-arrow"></div>
                <div className="row no-gutter">
                  <div className="col-lg-5 mb-4 mb-lg-0">
                    <p className="font-weight-bold mb-2 text-primary text-small">2015 - 2018 </p>
                    <h2 className="h5 mb-0 text-uppercase">BSIT</h2>
                    <p className="text-small mb-0">ABE International Business School</p><span className="small text-muted">East Grace Park, Caloocan</span>
                  </div>
                  <div className="col-lg-7">
                    <p className="text-muted">Graduated Bachelor of Science in Information Technology.</p>
                    <span className="text-muted small">Best in Capstone (Arduino).</span>
                    <p className="text-muted small">Champion in Table Tennis (Men Singles).</p>
                  </div>
                </div>
              </li>
              {/* Timeline item */}
              <li className="timeline-item ml-3 pb-4">
                <div className="timeline-arrow"></div>
                <div className="row no-gutter">
                  <div className="col-lg-5 mb-4 mb-lg-0">
                    <p className="font-weight-bold mb-2 text-primary text-small">2013 - 2014 </p>
                    <h2 className="h5 mb-0 text-uppercase">Automotive Mechanic</h2>
                    <p className="text-small mb-0">Lorraine Technical School</p><span className="small text-muted">11th Ave, Caloocan</span>
                  </div>
                  <div className="col-lg-7">
                    <p className="text-muted">Studied Automotive Electrical and Mechanical with Diesel and Gasoline Engine.</p>
                  </div>
                </div>
              </li>
              {/* Timeline item */}
              <li className="timeline-item ml-3 pb-4">
                <div className="timeline-arrow"></div>
                <div className="row no-gutter">
                  <div className="col-lg-5 mb-4 mb-lg-0">
                    <p className="font-weight-bold mb-2 text-primary text-small">2009 - 2012</p>
                    <h2 className="h5 mb-0 text-uppercase">High School</h2>
                    <p className="text-small mb-0">Tinajeros National High School</p><span className="small text-muted">Tinajeros, Malabon</span>
                  </div>
                  <div className="col-lg-7">
                    <p className="text-muted">Studied High School.</p>
                  </div>
                </div>
              </li>
              {/* Timeline item */}
              <li className="timeline-item ml-3 pb-4">
                <div className="timeline-arrow"></div>
                <div className="row no-gutter">
                  <div className="col-lg-5 mb-4 mb-lg-0">
                    <p className="font-weight-bold mb-2 text-primary text-small">2006 - 2008</p>
                    <h2 className="h5 mb-0 text-uppercase">Elementary</h2>
                    <p className="text-small mb-0">Tinajeros Elementary School</p><span className="small text-muted">Tinajeros, Malabon</span>
                  </div>
                  <div className="col-lg-7">
                    <p className="text-muted">Transferred to Tinajeros Elementary School from Casupanan Elementary School, Bataan.</p>
                  </div>
                </div>
              </li>
	        </ul>
	      </div>
	    </section>
		)
}