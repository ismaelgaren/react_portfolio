import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';

export default function Header(){

	const facebook = <FontAwesomeIcon icon={faFacebookF} />
	const twitter = <FontAwesomeIcon icon={faTwitter} />
	const linkedIn = <FontAwesomeIcon icon={faLinkedinIn} />
	const instagram = <FontAwesomeIcon icon={faInstagram}/>
	
	return(
		<>
		<section className="hero bg-cover bg-center mt-5" id="hero" style={{background: "url(../author.jpg)"}}>
          <div className="container py-5 my-5 index-forward">
            <div className="row">
              <div className="col-md-8 text-white">
                <h2 className="h4 text-primary font-weight-normal mb-0">Hi, I am</h2>
                <h1 className="text-uppercase text-xl mb-0"> Ismael <span className="text-primary"> Garen </span></h1>
                <h2 className="h4 font-weight-normal mb-5">Full Stack <span className="text-primary">Web Developer</span></h2>
                <p className="text-shadow">'"Dreaming big is the only way to achieve big, if you fail then you can try again but if you don’t aim high, then you will certainly not achieve something huge."'</p>
              </div>
            </div>
          </div>
        </section>
        
        <section className="container text-center section-padding-y">
	        <div className="row px-4">
	          <div className="col-lg-7 mx-auto">
	            <h2 className="text-uppercase mb-0">Ismael Garen </h2>
	            <h6 className="text-primary text-uppercase mb-0 letter-spacing-3">Full Stack Web Developer</h6>
	            <p className="text-muted my-4">Giving up is not in my options.</p>
	            <ul className="list-inline mb-0">
	              <li className="list-inline-item"><a className="social-link" href="https://www.facebook.com/jovygaren/">{facebook}</a></li>
	              <li className="list-inline-item"><a className="social-link" href="/#">{twitter}</a></li>
	              <li className="list-inline-item"><a className="social-link" href="https://www.linkedin.com/in/ismael-garen-3377711b9/">{linkedIn}</a></li>
	              <li className="list-inline-item"><a className="social-link" href="https://www.instagram.com/">{instagram}</a></li>
	            </ul>
	          </div>
	        </div>
	    </section>
	    </>


		)
}