import React from 'react';

export default function Project(){
	return(
		<section className="bg-light" id="education">
	      <div className="container">
	        <header className="mb-3 pb-4">
	          <p className="font-weight-bold text-primary text-uppercase letter-spacing-3">On-going.</p>
	          <h2 className="text-uppercase lined">Projects</h2>
	        </header>

	        {/* Timeline item */}
	        <ul className="timeline">
        		{/* Timeline item */}
    		   <li className="timeline-item ml-3 pb-4">
                <div className="timeline-arrow"></div>
                <div className="row no-gutter">
                  <div className="col-lg-5 mb-4 mb-lg-0">
                    <p className="font-weight-bold mb-2 text-primary text-small">2020 - 2021 </p>
                    <h2 className="h5 mb-0 text-uppercase">Course Booking System</h2>
                    <span className="small text-muted">HTML5, CSS, JavaScript, REST API, and MongoDB</span>
                  </div>
                  <div className="col-lg-7">
                    <p className="text-muted"><a href="https://ismaelgaren.gitlab.io/cs2_client_public">https://ismaelgaren.gitlab.io/cs2_client_public</a></p>
                  </div>
                </div>
              </li>
              <li className="timeline-item ml-3 pb-4">
                <div className="timeline-arrow"></div>
                <div className="row no-gutter">
                  <div className="col-lg-5 mb-4 mb-lg-0">
                    <p className="font-weight-bold mb-2 text-primary text-small">2020 - 2021 </p>
                    <h2 className="h5 mb-0 text-uppercase">JO-ES Publishing House Inc. Full Stack Website</h2>
                    <span className="small text-muted">HTML, CSS, JavaScript, NextJS, REST API, and MongoDB</span>
                  </div>
                  <div className="col-lg-7">
                    <p className="text-muted"><a href="https://ismaelgaren.gitlab.io/joes/">https://ismaelgaren.gitlab.io/joes/</a></p>
                  </div>
                </div>
              </li>
	        </ul>
	      </div>
	    </section>
		)
}