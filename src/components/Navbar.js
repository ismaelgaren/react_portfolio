import React from 'react';
import Button from 'react-bootstrap/Button';

export default function NavBar(){
    return(
        <>
        <header className="header">
          <nav className="navbar navbar-expand-lg fixed-top">
          
            <div className="container"><a className="navbar-brand" href="/#"><img src="../logo.svg" alt="" width="45" /></a>
              <Button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i className="fas fa-bars"></i></Button>
              <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item"><a className="nav-link" href="/">Home</a></li>
                  <li className="nav-item"><a className="nav-link" href="/about">About</a></li>
                  <li className="nav-item"><a className="nav-link" href="/skills">Skills</a></li>
                  <li className="nav-item"><a className="nav-link" href="/education">Education</a></li>
                  <li className="nav-item"><a className="nav-link" href="/experience">Experience</a></li>
                  <li className="nav-item"><a className="nav-link" href="/project">Projects</a></li>
                  <li className="nav-item"><a className="nav-link" href="/contact">Contact</a></li>
                </ul>
              </div>
            </div>
            
          </nav>
        </header>
        </>
    )
}
